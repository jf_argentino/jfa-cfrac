/**************************************************************************//**
 * @file continued_fraction.c
 * @author jf.argentino@free.fr
 * @version 1.290
 * @date 2013-09-25
 *
 * This file is the property of JF Argentino
 ******************************************************************************/
#include <limits.h>

#define ABSOLUTE_VALUE_OF_LDBL(x) (((x) > 0.0L)?(x):(-x))

/******************************************************************************
 * TODO How to choose epsilon?
 ******************************************************************************/
int double_to_continued_fraction (long double x, long long* a, int a_length,
                                  long double epsilon) {
   int n = 0;
   epsilon = ABSOLUTE_VALUE_OF_LDBL(epsilon);

   while (n < a_length) {
      a[n] = ((long long) x);
      x -= (long double) a[n];
      n ++;
      if (ABSOLUTE_VALUE_OF_LDBL(x) <= epsilon) break;
      x = 1.0L / x;
      if (x > ((long double) LLONG_MAX)) break;
   }
   return n;
}

/******************************************************************************
 * TODO Any way to optimize it?
 ******************************************************************************/
int continued_fraction_to_rational (const long long* a, int a_length,
                                    long long* num, long long* den) {
   long long nd[2][2];
   int n;
   if (a_length <= 0) {
      *num = 1;
      *den = 0;
      return 0;
   }
   nd[0][0] = a[0];
   nd[0][1] = 1LL;
   nd[1][0] = 1LL;
   nd[1][1] = 0LL;
   *num = nd[0][0];
   *den = nd[1][0];
   for (n = 1; n < a_length; n++) {
      *num = a[n] * nd[0][0] + nd[0][1];
      *den = a[n] * nd[1][0] + nd[1][1];
      nd[0][1] = nd[0][0];
      nd[1][1] = nd[1][0];
      nd[0][0] = *num;
      nd[1][0] = *den;
   }
   return n;
}

/******************************************************************************
 * TODO Security stop condition especially 'if (x == 0)'
 ******************************************************************************/
int double_to_rational (long double x, long long* num, long long* den,
                        long long maxnum, long long maxden) {
   
   int n = 0, posnum = 1, posden = 1;
   long long nd[2][2], an = 0LL, absnum, absden = 1LL;
   
   /*maxnum = maxnum > 0LL ? maxnum : -maxnum;
     maxden = maxden > 0LL ? maxden : -maxden;
   */

   nd[0][0] = 1LL;
   nd[0][1] = 0LL;
   nd[1][0] = 0LL;
   nd[1][1] = 1LL;
   *num = nd[0][0];
   *den = nd[1][0];
   an = ((long long) x);
   absnum = an;

   while ((maxnum > 0 ? (absnum < maxnum) : 1)
               && (maxden > 0 ? (absden < maxden) : 1)) {
      /* Update */
      n ++;
      *num = posnum ? absnum : -absnum;
      *den = posden ? absden : -absden;
      nd[0][1] = nd[0][0];
      nd[1][1] = nd[1][0];
      nd[0][0] = *num;
      nd[1][0] = *den;

      /* Security stop conditions */
      x -= (long double) an;
      if (x == 0) break; /* TODO */
      x = 1.0L / x;
      if (x > ((long double) LLONG_MAX)) break;
      
      /* Process */
      an = ((long long) x);
      absnum = an * nd[0][0] + nd[0][1];
      posnum = absnum > 0 ? 1 : 0;
      absnum = absnum > 0 ? absnum : -absnum;
      absden = nd[1][0] * an + nd[1][1];
      posden = absden > 0 ? 1 : 0;
      absden = absden > 0 ? absden : -absden;
   }
   return n;
}

