/**************************************************************************//**
 * @file continued_fraction_test.c
 * @author jf.argentino@free.fr
 * @version 1.1086
 * @date 2013-09-25
 *
 * This file is the property of JF Argentino
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>
#include "continued_fraction.h"

#define MAX_NUM 32768LL
#define MAX_DEN MAX_NUM

static void print_continued_fraction (long long* a, int a_length) {
   int n;
   printf ("[ ");
   for (n = 0; n < a_length - 1; n++) {
      printf ("%lld, ", a[n]);
   }
   printf ("%lld ]", a[a_length - 1]);
}

#define A_LENGTH 256
#define EPSILON 1e-18L
#define NB 1000000

static void test_continued_fraction (long double x) {
   long double dt_us;
   long long a[A_LENGTH];
   long long num, den;
   struct timeval t0, t1;
   int n, nb;
   nb = 0;
   printf ("%Le = ", x);
   gettimeofday (&t0, NULL);
   while (nb < NB) {
        n = double_to_continued_fraction (x, a, A_LENGTH, x * EPSILON);
        nb ++;
   }
   gettimeofday (&t1, NULL);
   print_continued_fraction (a, n);
   dt_us = (t1.tv_sec - t0.tv_sec)*1e6L + (t1.tv_usec - t0.tv_usec)*1.0L;
   printf ("\n%d double_to_continued_fraction of %d rounds in %Leus\n",
           NB, n, dt_us);

   nb = 0;
   gettimeofday (&t0, NULL);
   while (nb < NB) {
        continued_fraction_to_rational (a, n, &num, &den);
        nb ++;
   }
   gettimeofday (&t1, NULL);
   dt_us = (t1.tv_sec - t0.tv_sec)*1e6L + (t1.tv_usec - t0.tv_usec)*1.0L;
   printf (" = %lld / %lld (error %Lg)\n", num, den,
           fabsl (x - ((long double)num) / ((long double)den)));
   printf ("\n%d continued_fraction_to_rational of %d rounds in %Leus\n",
           NB, n, dt_us);

   nb = 0;
   gettimeofday (&t0, NULL);
   while (nb < NB) {
        n = double_to_rational (x, &num, &den, MAX_NUM, MAX_DEN);
        nb ++;
   }
   gettimeofday (&t1, NULL);
   dt_us = (t1.tv_sec - t0.tv_sec)*1e6L + (t1.tv_usec - t0.tv_usec)*1.0L;
   printf ("With a max numerator and denominator of %lld, "
           "we have %lld / %lld after %d rounds (error %Lg)\n",
           (long long) MAX_DEN, num, den, n,
           fabsl (x - ((long double)num) / ((long double)den)));
   printf ("\n%d double_to_rational of %d rounds in %Leus\n",
           NB, n, dt_us);

}

/**
 * Just in case that _GNU_SOURCE not defined
 */
#ifndef M_El
   #warning Constants from math.h not defined
   #define M_El        2.7182818284590452353602874713526625L  /* e */
   #define M_LOG2El    1.4426950408889634073599246810018921L  /* log_2 e */
   #define M_LOG10El   0.4342944819032518276511289189166051L  /* log_10 e */
   #define M_LN2l      0.6931471805599453094172321214581766L  /* log_e 2 */
   #define M_LN10l     2.3025850929940456840179914546843642L  /* log_e 10 */
   #define M_PIl       3.1415926535897932384626433832795029L  /* pi */
   #define M_PI_2l     1.5707963267948966192313216916397514L  /* pi/2 */
   #define M_PI_4l     0.7853981633974483096156608458198757L  /* pi/4 */
   #define M_1_PIl     0.3183098861837906715377675267450287L  /* 1/pi */
   #define M_2_PIl     0.6366197723675813430755350534900574L  /* 2/pi */
   #define M_2_SQRTPIl 1.1283791670955125738961589031215452L  /* 2/sqrt(pi) */
   #define M_SQRT2l    1.4142135623730950488016887242096981L  /* sqrt(2) */
   #define M_SQRT1_2l  0.7071067811865475244008443621048490L  /* 1/sqrt(2) */
#endif

static int demo_continued_fraction (void) {
   test_continued_fraction (M_El);
   test_continued_fraction (M_LOG2El);
   test_continued_fraction (M_LOG10El);
   test_continued_fraction (M_LN2l);
   test_continued_fraction (M_LN10l);
   test_continued_fraction (M_PIl);
   test_continued_fraction (M_PI_2l);
   test_continued_fraction (M_PI_4l);
   test_continued_fraction (M_1_PIl);
   test_continued_fraction (M_2_PIl);
   test_continued_fraction (M_2_SQRTPIl);
   test_continued_fraction (M_SQRT2l);
   test_continued_fraction (M_SQRT1_2l);
   test_continued_fraction (-3.245L);
   test_continued_fraction (-powl (2.0L, -32.0L));
   test_continued_fraction (powl (2.0L, 32.0L));
   test_continued_fraction (123357.6763411231L);
   return EXIT_SUCCESS;
}

static int printf_precision (long double x, int precision) {
   printf ("%.*Le %.*Lf %.*Lg\n", precision, x, precision, x, precision, x);
}

static int test_printf_precision (void) {
   char const * PI_STR = "3.1415926535897932384626433832795029";
   int NMAX = strlen(PI_STR);
   char str[NMAX+1];
   str[NMAX] = '\0';
   for (int p = 3; p < NMAX-2; p++) {
       snprintf (str, NMAX, "%.*Lf", p, M_PIl);
       int k = 0;
       while ((k < p+2) && (str[k] == PI_STR[k])) {
           k ++;
       }
       //printf (">>> %c ### %c <<< %d\n", str[k], PI_STR[k], k);
       char ok = '#';
       if (k == p+2) {
           ok = '=';
       } else if ((k == p+1) && (PI_STR[p+2] > '4')) {
           ok = '=';
       }
       printf ("%*s\n", k+5, k == p+2 ? "" : "v");
       printf ("%2d: %s\n", p, str);
       printf ("   %c%.*s%c\n", ok, p+2, PI_STR, PI_STR[p+2]);
   }
   return EXIT_SUCCESS;
}

int main (int argc, char** argv) {
    long double x, dx;
    long long num, den;
    long long max_num = MAX_NUM;
    long long max_den = MAX_DEN;
    int opt, k, round;

    while ((opt = getopt (argc, argv, "n:d:ht")) != -1) {
        switch (opt) {
            
            case 'n':
            max_num = atoll (optarg);
            break;
            
            case 'd':
            max_den = atoll (optarg);
            break;

            default:
            case 'h':
            printf ("%s [-n MAX_NUM] [-d MAX_DEN] x [y...]\n", argv[0]);
            printf ("   Diophantine approximation of any given real numbers\n");
            exit (opt == 'h' ? EXIT_SUCCESS : EXIT_FAILURE);
            
            case 't':
            return test_printf_precision ();
            return demo_continued_fraction ();
        }
    }
    for (k = optind; k < argc; k++) {
        x     = strtold (argv[k], NULL);
        round = double_to_rational (x, &num, &den, max_num, max_den);
        dx    = ((long double)num) / ((long double)den) - x;
        printf ("%.30Lg = %lld/%lld %+Lg [%d];\n", x, num, den, dx, round);
    }
    return 0;
}

