/**************************************************************************//**
 * @file continued_fraction.h
 * @author jf.argentino@free.fr
 * @version 1.941
 * @date 2013-09-25
 *
 * This file is the property of JF Argentino
 ******************************************************************************/
#ifndef CONTINUED_FRACTION_H
#define CONTINUED_FRACTION_H

/* pi < 355/113 < pi + 1/3748688 */
#define PI_NUM 355
#define PI_DEN 113

/* e > 2721/1001 */
#define E_NUM 2721
#define E_DEN 1001

/******************************************************************************
 * Diophantine decomposition of a number
 * x = a[0] + 1/(a[1] + 1/(a[2] + 1/(a[3] + ...)))
 * TODO How to choose epsilon from x? 1e-6*x looks good but...
 ******************************************************************************/
int double_to_continued_fraction (long double x, long long* a, int a_length,
                                  long double epsilon);

/******************************************************************************
 * If a[] is a continued fraction development, we have the relationship:
 *
 *  | num[n] num[n-1] |     | a[0] 1 |   | a[1] 1 |   | a[2] 1 |
 *  |                 |  =  |        | x |        | x |        | x ...
 *  | den[n] den[n-1] |     |  1   0 |   |  1   0 |   |  1   0 |
 *
 * then "num[n] / den[n]" is the best rational approximation of a[]
 ******************************************************************************/
int continued_fraction_to_rational (const long long* a, int a_length,
                                    long long* num, long long* den);

/******************************************************************************
 * Make the same thing than chaining 'double_to_continued_fraction' and
 * 'continued_fraction_to_rational' but with constraint on the max
 * value the numerator and/or the denominator can take. A zero or less
 * value disable the constraint BUT you should not disable the two alltogether
 * since for now the stop conditions are unsafe.
 ******************************************************************************/
int double_to_rational (long double x, long long* num, long long* den,
                        long long maxnum, long long maxden);

#endif

