#include "ulonglong_ops.h"
#include <stdio.h>

int main (int argc, char** argv) {
   unsigned long long umin = 1ULL;
   unsigned long long vmin = 1ULL;
   unsigned long long umax = 32ULL;
   unsigned long long vmax = 32ULL;
   unsigned long long u, v;
   for (u = umin; u < umax; u++) {
      for (v = vmin; v < vmax; v++) {
         printf ("GCD(%llu, %llu) = %llu, LCM(%llu, %llu) = %llu;\n",
                 u, v, ulonglong_gcd (u, v), u, v, ulonglong_lcm (u, v));
      }
   }
   return (0);
}

