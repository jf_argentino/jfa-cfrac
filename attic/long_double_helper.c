#include <limits.h>
#include <string.h>
#include <float.h>
#include <stdio.h>
#define __USE_GNU
#include <math.h>
#undef __USE_GNU

#define LDBL_EXPONANT_OFFSET -16383L
#define LDBL_EXPONANT_MASK 0x7fffffffL
#define LDBL_MANTISSA_EXPONANT -63L
#define ULLONG_LAST_BIT_MASK 1ULL
int decompose_long_double (long double x, unsigned long long* mantissa,
                           long* exponent2) {
   union {
      long double ld;
      struct {
         unsigned long long mantissa;
         long exponent;
      } ll;
   } y;
   if (x == 0.0L) {
      *mantissa = 0ULL;
      *exponent2 = 0L;
      return 0;
   }
   y.ld = x;
   *exponent2 = (y.ll.exponent & LDBL_EXPONANT_MASK)
                           + LDBL_EXPONANT_OFFSET + LDBL_MANTISSA_EXPONANT;
   *mantissa = y.ll.mantissa;
   while ((*mantissa != 0ULL) && (*mantissa & ULLONG_LAST_BIT_MASK) == 0ULL) {
      *mantissa = *mantissa >> 1;
      (*exponent2) ++;
   }
   return (x > 0) ? +1 : -1;
}

void print_char12 (unsigned char* c) {
   printf ("0x ");
   for (int n = 11; n >= 0; n--) {
      printf ("%02x ", c[n]);
   }
}

int main (void) {
   long long a[A_LENGTH];
   int n;
   long double x;
   unsigned long long mantissa;
   long exponent2;

   x = M_PIl;
   memset (a, 0, A_LENGTH * sizeof (long long));
   n = continued_fraction (x, a, A_LENGTH, EPSILON);
   printf ("%Le = ", x);
   print_contined_fraction (a, n);
   printf ("\n\n");
   
   x = 3.245L;
   memset (a, 0, A_LENGTH * sizeof (long long));
   n = continued_fraction (x, a, A_LENGTH, EPSILON);
   printf ("%Le = ", x);
   print_contined_fraction (a, n);
   printf ("\n\n");
   
   x = M_SQRT2l;
   memset (a, 0, A_LENGTH * sizeof (long long));
   n = continued_fraction (x, a, A_LENGTH, EPSILON);
   printf ("%Le = ", x);
   print_contined_fraction (a, n);
   printf ("\n\n");
   
   printf ("Size of long double: %u\n\n", sizeof (long double));
   x = 0.0L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   x = 1.0L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   x = -1.0L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   x = 2.0L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   x = -2.0L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   x = -0.125L;
   decompose_long_double (x, &mantissa, &exponent2);
   printf ("%4.4Lg = %c%llu * 2^%ld\n", x,
           decompose_long_double (x, &mantissa, &exponent2) > 0 ? '+' : '-',
           mantissa, exponent2);
   
   return 0;
}

