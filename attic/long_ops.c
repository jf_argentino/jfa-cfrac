#include <limits.h>

/******************************************************************************
 *
 ******************************************************************************/
#define UNLIKELY(x) __builtin_expect((x),0)
unsigned long LONG_OPS_OVERFLOW = 0UL;
unsigned long LONG_OPS_PRECISION = 0UL;

/******************************************************************************
 * abs ('num') must be greater than 'den', both are greater than zero
 ******************************************************************************/
static inline long long_neg_divide (long num, long den) {
   return (num / den) - ((-num % den) > (den / 2L) ? 1L : 0L);
}

/******************************************************************************
 *
 ******************************************************************************/
static inline long long_pos_divide (long num, long den) {
   return (num / den) + ((num % den) > (den / 2L) ? 1L : 0L);
}

/******************************************************************************
 *
 ******************************************************************************/
static inline long long_neg_mult_and_div (long x, long num, long den) {
   long rem;
   if (UNLIKELY(x <= (LONG_MIN / num))) {
      /* Divide first but keep the remainder */
      LONG_OPS_PRECISION ++;
      rem = -x % den;
      x /= den;
      if (UNLIKELY(x <= (LONG_MIN / num))) {
         LONG_OPS_OVERFLOW ++;
         return LONG_MIN;
      }
      return x * num - long_pos_divide (num * rem, den);
   }
   return long_neg_divide (x * num, den);
}

/******************************************************************************
 *
 ******************************************************************************/
static inline long long_pos_mult_and_div (long x, long num, long den) {
   long rem;
   if (UNLIKELY(x >= (LONG_MAX / num))) {
      /* Divide first but keep the remainder */
      LONG_OPS_PRECISION ++;
      rem = x % den;
      x /= den;
      if (UNLIKELY(x >= (LONG_MAX / num))) {
         LONG_OPS_OVERFLOW ++;
         return LONG_MAX;
      }
      return x * num + long_pos_divide (num * rem, den);
   }
   return long_pos_divide (x * num, den);
}

/******************************************************************************
 *
 ******************************************************************************/
long long_mult_and_div (long x, long num, long den) {
   if (UNLIKELY(x == 0L)) return 0L;
   if (UNLIKELY(num == 0L)) return 0L;
   if (x < 0L) {
      return long_neg_mult_and_div (x, num, den);
   }
   return long_pos_mult_and_div (x, num, den);
}

