#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "continued_fraction.h"

#define DEFAULT_MAX_NUM 1024LL
#define DEFAULT_MAX_DEN 1024LL

int main (int argc, char** argv) {
   long double x;
   long long max_num = DEFAULT_MAX_NUM, num;
   long long max_den = DEFAULT_MAX_DEN, den;
   int opt, n;
   
   while ((opt = getopt (argc, argv, "n:d:h")) != -1) {
      switch (opt) {
         case 'n':
         max_num = atoll (optarg);
         break;
         case 'd':
         max_den = atoll (optarg);
         break;
         default:
         case 'h':
         printf ("%s [-n MAX_NUM] [-d MAX_DEN] x [y...]\n", argv[0]);
         exit (opt == 'h' ? EXIT_SUCCESS : EXIT_FAILURE);
      }
   }
   for (n = optind; n < argc; n++) {
      x = strtold (argv[n], NULL);
      double_to_rational (x, &num, &den, max_num, max_den);
      printf ("%Lg = %lld / %lld\n", x, num, den);
   }
   exit (EXIT_SUCCESS);
}

