#ifndef ULONGLONG_OPS_H
#define ULONGLONG_OPS_H

unsigned long long ulong2ulonglong (unsigned long x);
//unsigned long ulonglong2ulong (unsigned long long x);
unsigned long long ulonglong_gcd (unsigned long long u, unsigned long long v);
unsigned long long ulonglong_lcm (unsigned long long u, unsigned long long v);

#endif

