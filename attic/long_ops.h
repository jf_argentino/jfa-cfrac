#ifndef LONG_OPS_H
#define LONG_OPS_H

extern unsigned long LONG_OPS_OVERFLOW;
unsigned long LONG_OPS_PRECISION;

long long_mult_and_div (long x, long num, long den);

#endif

