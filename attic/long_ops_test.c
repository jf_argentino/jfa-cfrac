#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "long_ops.h"
#include <math.h>

static void test_long_mult_and_div (long x, long num, long den) {
   long y1, y2;
   long double xf, yf, numf, denf, err1, err2;
   long long xl, yl, numl, denl, err1l, err2l;
   LONG_OPS_OVERFLOW = 0;
   LONG_OPS_PRECISION = 0;
   y1 = long_mult_and_div (x, num, den);
   y2 = (x * num) / den;
   xf = (long double) x;
   numf = (long double) num;
   denf = (long double) den;
   yf = xf * (numf / denf);
   err1 = ((long double) y1) - yf;
   err2 = ((long double) y2) - yf;
   xl = (long long) x;
   numl = (long long) num;
   denl = (long long) den;
   yl = (xl * numl) / denl;
   err1l = ((long long) y1) - yl;
   err2l = ((long long) y2) - yl;
   printf ("%ld * %ld / %ld = %ld (err = %Lf%s%s) = %ld (err = %Lf) "
           "= %lld (err = %lld / %lld)%s%s\n",
           x, num, den, y1, err1,
           LONG_OPS_OVERFLOW?" OVERFLOW":"",
           LONG_OPS_PRECISION?" PRECISION":"", y2, err2, yl, err1l, err2l,
           fabsl (err1) > fabsl (err2) ? " *************" : "",
           llabs (err1l) > llabs (err2l) ? " #############" : "");
}

#define NUM_MAX 20
#define DEN_MAX 50
int main (int argc, char** argv) {
   long x, num, den;
   int n;
   for (n = 1; n < argc; n++) {
      x = atoi (argv[n]);
      for (num = 2L; num < NUM_MAX; num++) {
         for (den = 2L; den < DEN_MAX; den++) {
            test_long_mult_and_div (x, num, den);
         }
      }
      printf ("\n");
   }
   return 0;
}

