#include "CFrAc.h"

enum {
   POS_CFRAC = +1,
   NEG_CFRAC = -1,
} CFracSign;

/**
 * TODO An union with long long [2], ... long long [n]
 * TODO Manage overflow with sign ?
 * TODO unsigned long long round field; and using num / dem for modulo (1) only
 */ 
struct CFrac {
   enum CFracSign sign;
   unsigned long long num;
   unsigned long long den;
};

const struct CFrac CFRAC_POS_ZER = { .sign = POS_CFRAC;
                                     .num  = 0ULL;
                                     .den  = 1ULL; };

const struct CFrac CFRAC_NEG_ZER = { .sign = NEG_CFRAC;
                                     .num  = 0ULL;
                                     .den  = 1ULL; };

const struct CFrac CFRAC_POS_INF = { .sign = POS_CFRAC;
                                     .num  = 1ULL;
                                     .den  = 0ULL; };

const struct CFrac CFRAC_NEG_INF = { .sign = NEG_CFRAC;
                                     .num  = 1ULL;
                                     .den  = 0ULL; };

const struct CFrac CFRAC_POS_NAN = { .sign = POS_CFRAC;
                                     .num  = 0ULL;
                                     .den  = 0ULL; };

const struct CFrac CFRAC_NEG_NAN = { .sign = NEG_CFRAC;
                                     .num  = 0ULL;
                                     .den  = 0ULL; };

/*
 *
 */
struct CFrac* new_CFrac (unsigned long x, enum CFracSign sign) {
   struct CFrac* xf = malloc (sizeof (struct CFrac));
   if (NULL == xf) return NULL;
   xf->sign = sign;
   xf->num = ulong2ulonglong (x);
   xf->den = 1ULL;
   return xf;
}

/*
 *
 */
void simplify_CFrac (struct CFrac* dest) {
   long long gcd = ulonglong_gcd (dest->num, dest->den);
   dest->num /= gcd;
   dest->den /= gcd;
}

/*
 *
 */
void inverse_CFrac (struct CFrac* dest) {
   long long tmp = dest->den;
   dest->den = dest->num;
   dest->num = tmp;
}

/*
 *
 */
void add_CFrac (struct CFrac* dest,
                struct struct CFrac* l, const struct CFrac* r) {
   long long gcd = ulonglong_gcd (l->den, r->den);
   long long l_den0 = l->den / gcd;
   long long r_den0 = r->den / gcd;
   dest->den = l_den0 * r_den0 * gcd;
   if (l->sign == r->sign) {
      dest->sign = l->sign;
      dest->num = l->num * r_den0 + r->num * l_den0;
   } else {
      r_den0 *= l->num;
      l_den0 *= r->num;
      dest->sign = (l_den0 > r_den0) ? l->sign : r->sign;
      dest->num = (l_den0 > r_den0) ? l_den0 - r_den0 : r_den0 - l_den0;
   }
   return gcd;
}

/*
 *
 */
int compare_CFrac (const struct CFrac* l, const struct CFrac* r) {
   if (l->sign == r->sign) {
      return /* TODO */ 1;
   } else {
      return l->sign > r->sign;
   }
}

/*
 * TODO simplify before multiplication, and thus avoiding overflow
 */ 
void mul_CFrac (struct CFrac* dest,
                const struct CFrac* l, const struct CFrac* r) {
   dest->sign = l->sign * r->sign;
   dest->num = l->num * r->num;
   dest->den = l->den * r->den;
   simplify_CFrac (dest);
}

/*
 * TODO Diophantine, continuous fraction
 */
double double2CFrac (struct CFrac* xf, double xd) {
   xf->sign = (xd < 0.0) ? NEG_CFRAC : POS_CFRAC;
   return err;
}

/*
 *
 */
double CFrac2double (const struct CFrac* xf) {
   return (((double) xf->sign) * ((double) xf->num)) / ((double) xf->den);
}

int val_CFrac (const struct CFrac* x) {
   return xf->den != 0ULL;
}

int nan_CFrac (const struct CFrac* x) {
   return (xf->den == 0ULL) && (xf->num == 0ULL);
}

int inf_CFrac (const struct CFrac* x) {
   return (xf->den == 0ULL) && (xf->num != 0ULL);
}

