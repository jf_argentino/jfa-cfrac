#include "ulonglong_ops.h"
#include <linux/types.h>

const unsigned int CHAR_SHIFT = 3U;
const size_t SIZEOF_LONG_LONG = sizeof(long long);
#define LONG_LONG_SHIFT (SIZEOF_LONG_LONG << CHAR_SHIFT)

unsigned long long ulong2ulonglong (unsigned long x) {
   return ((unsigned long long)x) << LONG_LONG_SHIFT;
}

/*unsigned long ulonglong2ulong (unsigned long long x) {
   return ();
}*/

unsigned long long ulonglong_gcd (unsigned long long u, unsigned long long v) {
   long shift;
   long diff;
   /* GCD(0,x) := x */
   if (u == 0ULL || v == 0ULL) return u | v; 
   /* Let shift := lg K, where K is the greatest power of 2
      dividing both u and v. */
   for (shift = 0; ((u | v) & 1ULL) == 0ULL; ++shift) {
       u >>= 1;
       v >>= 1;
   }
   while ((u & 1ULL) == 0ULL) u >>= 1;
   /* From here on, u is always odd. */
   do {
       while ((v & 1ULL) == 0ULL)  /* Loop X */
         v >>= 1;
         /* Now u and v are both odd, so diff(u, v) is even.
          Let u = min(u, v), v = diff(u, v)/2. */
       if (u <= v) {
           v -= u;
       } else {
           diff = u - v;
           u = v;
           v = diff;
       }
       v >>= 1;
   } while (v != 0ULL);
   return u << shift;
}

unsigned long long ulonglong_lcm (unsigned long long u, unsigned long long v) {
   return (u < v) ? v * (u / ulonglong_gcd (u, v))
                  : u * (v / ulonglong_gcd (u, v));
}

